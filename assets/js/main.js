jQuery(document).ready(function($) {
	
	Dropzone.autoDiscover = false;


	if ( $('div#myDropzone').length ) {
		var myDropzone = new Dropzone("div#myDropzone", { url: "#"});
	};

	if ( $('div#my-awesome-dropzone').length ) {
		var myAwesomeDropzone = new Dropzone("div#my-awesome-dropzone", { url: "#"});
	};
		
	$('.single .ui.dropdown').dropdown();

	$('.ui.dropdown').dropdown();


	$('.multiple-tag .ui.dropdown').dropdown();

	$('.menu-style .ui.dropdown').dropdown();

	$('.selection .ui.dropdown').dropdown();

 
	if ( $('#summernote').length ) { 

		$('#summernote').summernote();
	};


	$('#select-with-optgroups').dropdown();

	

	$(function () {
	  $('[data-toggle="tooltip"]').tooltip({
	  	container: 'body'
	  })
	})
	// $('input[rel="txtTooltip"]').tooltip({
	//     container: 'body'
	// });


	if ($('.ui.checkbox').length) {

		$('.ui.checkbox')
		  .checkbox()
		;

	};

		if ($('input.datepicker').length)  {
			$("input.datepicker").daterangepicker();	
		};


		if ($('input.datepick').length)  {
			$("input.datepick").datetimepicker();	
		};



	$('.toggle').checkbox({
	    onChecked: function() {
	    	
	     $(this).siblings('.works-status').text('active workspace'); 
	    },
	    onUnchecked: function() {
	     $(this).siblings('.works-status').text('inactive workspace'); 
	      
	    },
	}); 

	$('.sidebar-gray-bg').height($('.create-workspace').height()+200); 


	$('#post-message').on('show.bs.modal', function (e) {
	  	console.log('clicked '); 
	  	// $("#my-awesome-dropzone").dropzone({ url: "/file/post" });
	})

	if ($('.password-change').length) {
		$('.password-change').on('click', function(event) {
			event.preventDefault();
			/* Act on the event */
			BootstrapDialog.alert({
            title: 'Password Change Request',
            message: 'You password has been update! When you click on “OK” you will be logout of the system.'
        });
		});
	};


	
$('.create-workspace-inner .page-sidebar').height($('.create-workspace-inner').height()+150 );

$(window).resize(function(event) {
		$('.create-workspace-inner .page-sidebar').height($('.create-workspace-inner').height()+150 );
});



if ($('.milestone').length) {
	
		
		var milstone = $(this);

		$('a.milestone-toggle').on('click', function(event) {
			
			

			event.preventDefault();
			var i =  $(this).children('i'); 
			var parentmilstone = $(this).parents('.milestone'); 

			console.log(parentmilstone); 

			if ( i.hasClass('icon-plus')) {
				 i.removeClass('icon-plus'); 
				 i.addClass('icon-minus'); 
				 
				parentmilstone.find('.milestone-collapse').slideDown('400');	
				// parentmilstone.find('.milestone-collapse').addClass('displayed');	

			} else { 
				 i.removeClass('icon-minus'); 
				 i.addClass('icon-plus'); 
				 parentmilstone.find('.milestone-collapse').slideUp('400');	
				// parentmilstone.find('.milestone-collapse').removeClass('displayed');	
			};


			// if (parentmilstone.find('.milestone-collapse').hasClass('displayed')) {
				
			// 	parentmilstone.find('.milestone-collapse').slideUp('400');	
			// 	parentmilstone.find('.milestone-collapse').removeClass('displayed');	

			// } else {
			// 	parentmilstone.find('.milestone-collapse').slideDown('400');	
			// 	parentmilstone.find('.milestone-collapse').addClass('displayed');	
			// };
			
		});



		
	
};

$('.filter a.toggle-btn').on('click', function(event) {
	event.preventDefault();
	var a = $(this); 
	console.log(a); 
	a.children('i.right').removeClass('fa-chevron-down').addClass('fa-chevron-up'); 

	

	if ( a.siblings('.custom-dropdown-menu').hasClass('displayed') ) {
		
		a.siblings('.custom-dropdown-menu').slideUp().removeClass('displayed');
		a.children('i.right').removeClass('fa-chevron-up').addClass('fa-chevron-down'); 
		


	} else { 
		a.siblings('.custom-dropdown-menu').slideDown().addClass('displayed');
		a.children('i.right').removeClass('fa-chevron-down').addClass('fa-chevron-up'); 
		
	};


});

$('.filter.sort ul li a').on('click', function(event) {
	event.preventDefault();
	var a = $(this); 
	console.log(a); 
	
	if ( a.children('i').hasClass('fa-arrow-up') ) {
		
		a.children('i').removeClass('fa-arrow-up').addClass('fa-arrow-down');


	} else { 
		a.children('i').removeClass('fa-arrow-down').addClass('fa-arrow-up');
	};


});

$('.filter.dropdown ul li a').on('click', function(event) {
	event.preventDefault();
	var a = $(this); 
	console.log(a); 

	if ( !a.hasClass('selected') ) {
		
		a.addClass('selected'); 
		a.children('i').hasClass('fa-plus').addClass('fa-check')

	} else { 
		a.removeClass('selected'); 
		a.children('i').removeClass('fa-check').addClass('fa-plus')
	};
	
	


});



	
	
	
});//on laod


  

